# Minimal example; see inventory.py for more.
from sllurp import llrp
from twisted.internet import reactor
import logging
logging.getLogger().setLevel(logging.INFO)
import time
import argparse
from websocket import create_connection
import json


parser = argparse.ArgumentParser()
parser.add_argument("-r", "--readerhost", help="reader address", default='169.254.10.1')
parser.add_argument("-rp", "--readerport", help="reader port", default=llrp.LLRP_PORT)
parser.add_argument("-dc", "--datacollectorhost", help="reader port", default='159.89.206.10')
parser.add_argument("-dcp", "--datacollectorport", help="reader port", default='8752')
args = parser.parse_args()

print(args.readerhost, args.readerport, args.datacollectorhost, args.datacollectorport)

connect_to = "ws://"+args.datacollectorhost+":"+args.datacollectorport
# print('connecting to: '+connect_to)
ws = create_connection(connect_to)

# # kalau mau buang ke socket lewat sini
def cb (tagReport):
    tags = tagReport.msgdict['RO_ACCESS_REPORT']['TagReportData']
    print(len(tags))
    for tag in tags:
        EPC_96 = tag['EPC-96'].decode("utf-8")
        antenna = tag['AntennaID'][0]
        rssi = tag['PeakRSSI'][0]
        seenTimeStamp = time.time()
        data = {'EPC_96':EPC_96, 'antennaID':antenna, 'RSSI':rssi, 'time':seenTimeStamp}
        payload = json.dumps(data)
        ws.send(payload)
        # print(payload)
#buat selanjutnya kalau mau pake reset
# def shutdown(factory):
#     return factory.politeShutdown()

factory = llrp.LLRPClientFactory(duration=0.5, tx_power=20, tag_population=400)
factory.addTagReportCallback(cb)
reactor.connectTCP(args.readerhost, args.readerport, factory)
reactor.run()